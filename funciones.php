<?php

define('URL_ORQUESTADOR', '192.168.0.108');

function get_home() {
  $titulo = 'Inicio';
  $contenido = 'It works!!!';
  print add_html_format($titulo, $contenido);
}

function get_balance($id_evento) {
  $data['id_evento'] = $id_evento;
  $data['balance'] = _get_balance_evento($id_evento);
  xml_printer($data);
}

function get_asiento($tipo) {
  $data = array();
  global $db;
  switch($tipo) {
    case 'evento':
      $data = data_asiento_evento();
      // Pendiente inteligencia del negocio
      if (!$data['es_error']) {
        $data['id_asiento'] = mt_rand(200, 300);
        $data['fecha'] = date("Y-m-d H:i:s", time());
      }
      break;
    case 'factura':
      $data = data_asiento_factura();
      // Pendiente inteligencia del negocio
      if (!$data['es_error']) {
        $data['id_asiento'] = mt_rand(200, 300);
        $data['fecha'] = date("%Y-%M-%d %H:%i:%s", time());
      }
      break;
    default:
      $data['es_error'] = 1;
      $data['mensaje_error'] = 'Tipo de asiento desconocido. ';
  }
  xml_printer($data);
}

function crear_asiento() {
  if (count($_GET)) {
    $variables = $_GET;
    unset($variables['tipo']);
    $url = 'http://' . URL_ORQUESTADOR . '/contabilidad/asiento/' . urlencode($_GET['tipo']) . encadenar_get();
    if ($xml = rest_request($url)) {
      var_dump($xml);
    }
  }
  $titulo = 'Crear Asiento';
  $contenido = '';
  $contenido .= '<form action="/asiento/crear" method="get">';
  $contenido .= '<table>';
  $contenido .= '<tr><td>Tipo de asiento</td><td><input name="tipo" type="radio" value="evento">Evento<br /><input name="tipo" type="radio" value="factura" checked>Factura</td></tr>';
  $contenido .= '<tr><td>ID evento</td><td><input name="id_evento" type="textfield"></td></tr>';
  $contenido .= '<tr><td>ID factura</td><td><input name="id_factura" type="textfield"></td></tr>';
  $contenido .= '<tr><td>ID usuario</td><td><input name="id_usuario" type="textfield"></td></tr>';
  $contenido .= '<tr><td>Valor</td><td><input name="valor" type="textfield"></td></tr>';
  $contenido .= '<tr><td>Notas</td><td><textarea name="notas"></textarea></td></tr>';
  $contenido .= '</table>';
  $contenido .= '<input type="submit" value="Crear Asiento">';
  $contenido .= '</form>';
  print add_html_format($titulo, $contenido);
}

function data_asiento_evento() {
  $data = array(
    'id_evento' => '',
    'fecha' => '',
    'valor' => '',
    'notas' => '',
    'es_error' => 0,
    'mensaje_error' => '',
  );
  if (isset($_GET['id_evento']) && is_numeric($_GET['id_evento'])) {
    $data['id_evento'] = (int) $_GET['id_evento'];
  }
  else {
    $data['es_error'] = 1;
    $data['mensaje_error'] .= 'El ID de evento debe ser un entero. ';
  }
  if (isset($_GET['valor']) && is_numeric($_GET['valor']) && $_GET['valor'] >= 0) {
    $data['valor'] = (int) $_GET['valor'];
  }
  else {
    $data['es_error'] = 1;
    $data['mensaje_error'] .= 'El valor del evento debe ser un número positivo. ';
  }
  if (isset($_GET['notas']) && is_numeric($_GET['notas'])) {
    $data['notas'] = (string) $_GET['notas'];
  }
  else {
    $data['notas'] = '';
  }
  return $data;
}

function data_asiento_factura() {
  $data = array(
    'id_evento' => '',
    'id_factura' => '',
    'id_usuario' => '',
    'fecha' => '',
    'valor' => '',
    'notas' => '',
    'es_error' => 0,
    'mensaje_error' => '',
  );
  if (isset($_GET['id_evento']) && is_numeric($_GET['id_evento'])) {
    $data['id_evento'] = (int) $_GET['id_evento'];
  }
  else {
    $data['es_error'] = 1;
    $data['mensaje_error'] .= 'El ID de evento debe ser un entero. ';
  }
  if (isset($_GET['id_factura']) && is_numeric($_GET['id_factura'])) {
    $data['id_factura'] = (int) $_GET['id_factura'];
  }
  else {
    $data['es_error'] = 1;
    $data['mensaje_error'] .= 'El ID de factura debe ser un entero. ';
  }
  if (isset($_GET['id_usuario']) && is_numeric($_GET['id_usuario'])) {
    $data['id_usuario'] = (int) $_GET['id_usuario'];
  }
  else {
    $data['es_error'] = 1;
    $data['mensaje_error'] .= 'El ID de usuario debe ser un entero. ';
  }
  if (isset($_GET['valor']) && is_numeric($_GET['valor']) && $_GET['valor'] >= 0) {
    $data['valor'] = (int) $_GET['valor'];
  }
  else {
    $data['es_error'] = 1;
    $data['mensaje_error'] .= 'El valor del evento debe ser un número positivo. ';
  }
  if (isset($_GET['notas']) && is_numeric($_GET['notas'])) {
    $data['notas'] = (string) $_GET['notas'];
  }
  else {
    $data['notas'] = '';
  }
  return $data;
}

function xml_printer($data) {
  header('Content-type: text/xml; charset=UTF-8');
  $xml = new XMLWriter;
  $xml->openMemory();
  $xml->setIndent(true);
//$xml->startDocument('1.0', 'UTF-8');
  $xml->startDocument('1.0', 'UTF-8');
  $xml->startElement('data');
  foreach ($data as $key => $value) {
    $xml->startElement($key);
    $xml->text($value);
    $xml->endElement();
  }
  $xml->endElement();
  $xml->endDocument();
  echo $xml->outputMemory(TRUE);
}

function rest_request($url) {
  try {
    $content = file_get_contents($url);
    $xml = new SimpleXMLElement($content);
    var_dump($xml);
    return $xml;
  } catch (Exception $e) {
    return FALSE;
  }
}

function encadenar_get() {
  $cadena = '';
  $variables = array();
  foreach ($_GET as $key => $value) {
    $variables[] = urlencode($key . '=' . $value);
  }
  if ($variables) {
    $cadena .= '?' . implode('&', $variables);
  }
  return $cadena;
}

function _get_balance_evento($id_evento) {
  return mt_rand();
}

function iniciar(&$link, &$app) {
  include_once 'mysql.class.php';
  include_once 'vista.php';
  require_once 'Slim/Slim.php';
  $app = new Slim();
  $link = mysql_connect('localhost', "diplomado", "diplomado");
  if (!$link) {
    exit('No se pudo conectar al servidor MySQL');
  }
  if (!mysql_select_db('diplomado')) {
    exit('No se pudo conectar a la base de datos');
  }
}

function terminar(&$link, &$app) {
  $app->run();
  mysql_close($link);
}
