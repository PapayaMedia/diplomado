<?php

include_once 'funciones.php';
include_once 'vista.php';
include_once 'Slim/Slim.php';
$link = mysql_connect('localhost', "diplomado", "diplomado");
$app = new Slim();
if (!$link) {
  exit('No se pudo conectar al servidor MySQL');
}
if (!mysql_select_db('diplomado')) {
  exit('No se pudo conectar a la base de datos');
}

//GET route
$app->get('/', function () {
  get_home();
});

$app->get('/balance/:evento', 'get_balance');
$app->get('/asiento/crear', 'crear_asiento');
$app->get('/asiento/:tipo', 'get_asiento');

//POST route
$app->post('/post', function () {
  echo 'This is a POST route';
});

//PUT route
$app->put('/put', function () {
  echo 'This is a PUT route';
});

//DELETE route
$app->delete('/delete', function () {
  echo 'This is a DELETE route';
});

$app->run();
mysql_close($link);
