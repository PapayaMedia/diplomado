<?php

function add_html_format($titulo, $contenido) {
  $output =
'<html>
  <head>
    <title>' . $titulo . ' | Aplicaci&oacute;n Contable | Env&iacute;os a Toda S.A.S.</title>
  </head>
  <body>
    <div id="contenido">
      <h1>' . $titulo . '</h1>
      ' . $contenido . '
    </div>
  </body>
</html>';
  return $output;
}

